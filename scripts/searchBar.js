/*
    This class contains the logic of the searchbar.
    With the searchbar, the user can look up (german) airports 
    or callsigns to jump there.
*/
function Searchbar ( ) {
    
    this.value = "";
    this.handled = true;

    this.airportCoords = undefined;
    this.airportHandled = true;

    // Eventlistener on searchbar
    document.getElementById("searchbar").addEventListener('change', (e) => {
        console.log("Searched for: " + e.currentTarget.value);

        this.value = String(e.currentTarget.value).trim();
        this.handled = false;
    });

    /*
        Checks if the searchinput has changed.

        Returns:    state: boolean      if its a new not handled search
    */
    this.isNewSearch = function() {
        return !this.handled;
    }

    /*
        Checks whether the input value is valid. Then an API get request is made
        to get the position. It is asynchly saved in the top class variable and then
        checked if it wasnt used.
    */
    this.searchAirport = async function() {
        if (!this.isNewSearch()) return undefined;

        if (this.value.length < 0 || this.value.length > 4 || hasNumber(this.value)) 
            return undefined;
        
        this.handled = true;

        // Make API call
        try {
            let url = "https://corsproxy.io/?https://www.airport-data.com/api/ap_info.json?icao=" + this.value;
            httpGet(url, 'json', false, (response) => {
                print(response)
                if (!(parseFloat(response["latitude"]) == 0 && parseFloat(response["longitude"]) == 0)) {
                    this.airportCoords = new Coordinate(parseFloat(response["latitude"]), parseFloat(response["longitude"]));
                } else {
                    console.log("SearchBar: Airport location coordinates invalid")
                }

                this.airportHandled = false;
            });
        } catch (error) {
            console.error("AirportController: Error when fetching API call", error);
        }

    }

    /*
        This method checks, if the search input is valid for
        a callsign syntax. 

        Returns:    value: string       search value
    */
    this.searchCallsign = function() {
        if (!this.isNewSearch()) return undefined;

        if (this.value.length < 0 || this.value.length > 7) 
            return undefined;

        this.handled = true;
        return (this.value.toUpperCase());
    }


    
}