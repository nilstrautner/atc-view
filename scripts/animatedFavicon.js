document.head = document.head || document.getElementsByTagName('head')[0];

const delay = (delayInms) => { return new Promise(resolve => setTimeout(resolve, delayInms)); };

// Changes the favicon
function changeFavicon(src) {
    var link = document.createElement('link'),
        oldLink = document.getElementById('dynamic-favicon');
    link.id = 'dynamic-favicon';
    link.rel = 'shortcut icon';
    link.href = src;
    if (oldLink) {
    document.head.removeChild(oldLink);
    }
    document.head.appendChild(link);
}

// Starts the animation, looping
async function start() {
    const t = 1000;
    const images = 6;
    let i = 0;
    while (true) {
        await delay(t);
        changeFavicon('../img/favicon/animated/icon' + i + '.ico');
        i++;
        i = i % (images);
    }
}

changeFavicon('../img/favicon/animated/icon0.ico');
start();