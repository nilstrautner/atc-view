/*
    This class handles all country borders and airspaces.
    It loads, draws and checks dynamically, which parts to render.
*/
function MapLines ( ) {
    
    // Countries
    this.countries = [];
    this.countriesMap = new Map();
    this.countriesCenter = [];
    this.currentCountries = [];
    
    // Airspaces
    this.airspaces = [];
    this.airspacesMap = new Map();
    this.renderableAirspaces = null; // sub part of airspaces
    // TODO Idee Verbesserung: im Interval checken, welche airspaces gerendert werden sollen, diese dann in renderableAirspaces stecken, nur diese rendern
    
    this.lastPosCheck = 0;
    this.lastPosCheckInterval = 1000;

    /*
        Loads all lines from a geojson file. It accesses the countries.geojson and 
        the contriesCenter.json files and saves its data in the variables.
    */
        this.loadCountries = function() {
            // Load countries
            fetch("config/airspace/countries.geojson")
                .then(response => {
                    if (!response.ok) throw new Error(`HTTP error! status: ${response.status}`);
                    return response.json();
                })
                .then(data => {
                    this.countries = data.features;
                    // Build map
                    for (const country of this.countries) {
                        this.countriesMap.set(country.properties.ADMIN, country);
                    }
                    print("MapLines: Loaded countries data")
                })
                .catch(error => {
                    console.error('Error loading the file:', error);
                });

            // Load countries center
            fetch("config/airspace/countriesCenter.json")
                .then(response => {
                    if (!response.ok) throw new Error(`HTTP error! status: ${response.status}`);
                    return response.json();
                })
                .then(data => {
                    this.countriesCenter = data;
                    print("MapLines: Loaded countries center data")
                })
                .catch(error => {
                    console.error('Error loading the file:', error);
                });
        }

    /*
        Loads all airspaces from local json and saves it to to this.airspaces.
    */
        this.loadAirspaces = function() {
            // Load the text file using Fetch API
            fetch("config/airspace/atc_positions.json")
            .then(response => {
                if (!response.ok) throw new Error(`HTTP error! status: ${response.status}`);
                return response.json();
            })
            .then(data => {
                this.airspaces = data;
                print("MapLines: Loaded airspaces data")
            })
            .catch(error => {
                console.error('Error loading the file:', error);
            });
        }

    /*
        This method draws all country borders. It only draws relevant
        visible countries. It utelizes the geojson format file structure.

        Parameters: baseCoordinates:    PVector    lat/long coordiantes at 0|0
                    coordinatePerPixel: double     similiar to zoom
    */
    this.displayCountries = function(baseCoordinates, coordinatePerPixel) {
        noFill();
        stroke(colorpicker_border.color());

        let totalLines = 0;
        const c = this.getVisibleCountries(baseCoordinates, coordinatePerPixel);
        for (const item of c) {
            // Center file has found country that is not in geojson borders
            if (!this.countriesMap.has(item)) {
                print(item, "not found")
                continue
            }

            const country = this.countriesMap.get(item);
            const shapes = country.geometry.coordinates;
            for (const shape of shapes) {
                beginShape();
                for (let part of shape) {
                    // check if its array
                    if (part.length == 2) {
                        const now = coordinatesToPixel(new Coordinate(part[1], part[0]), baseCoordinates, coordinatePerPixel);
                        
                        //if (this.isOnScreen(now)) { // TEMP: sometimes lines are over screen because outside vertexes are skipped
                            vertex(now.x, now.y);
                            totalLines++;
                        //}  
                        continue
                    }
                    
                    // Draw
                    endShape();
                    beginShape();
                    for (const coordinate of part) {
                        const now = coordinatesToPixel(new Coordinate(coordinate[1], coordinate[0]), baseCoordinates, coordinatePerPixel);
                        
                        //if (this.isOnScreen(now)) { // TEMP: sometimes lines are over screen because outside vertexes are skipped
                            vertex(now.x, now.y);
                            totalLines++;
                        //}        
                    }
                }
                endShape();
                // print("Lines drawed:", totalLines)
            }
        }
    }

    /*
        

        Parameters: baseCoordinates:    PVector    lat/long coordiantes at 0|0
                    coordinatePerPixel: double     similiar to zoom
    */
        this.displayAirspaces = function(baseCoordinates, coordinatePerPixel) {
            noFill();
            stroke(colorpicker_airspace.color());

            for (const airspace of this.airspaces) {
                beginShape();
                
                // Check first one
                if (airspace.map_region.length > 0) {
                    const first = coordinatesToPixel(new Coordinate(airspace.map_region[0].lat, airspace.map_region[0].lng), baseCoordinates, coordinatePerPixel);
                    if (!this.isNearbyScreen(first)) continue;
                }

                for (const coordinate of airspace.map_region) {
                    const now = coordinatesToPixel(new Coordinate(coordinate.lat, coordinate.lng), baseCoordinates, coordinatePerPixel);
                    vertex(now.x, now.y);
                }

                endShape(CLOSE);
            }
                          
        }

    /*
        Checks, if a position is inside the visible screen area with
        respect to a little padding.

        Parameter: pos   PVector   position in pixels
        Returns:   state boolean   if its inside screen
    */
    this.isOnScreen = function(pos) {
        const padding = 50;
        if (pos.x < -padding || pos.x > windowWidth+padding ||
            pos.y < -padding || pos.y > windowHeight+padding) return false;
        return true;
    }

    /*
        Checks, if a position is inside the visible screen area with
        respect to a big padding.

        Parameter: pos   PVector   position in pixels
        Returns:   state boolean   if its inside screen
    */
        this.isNearbyScreen = function(pos) {
            const padding = 800;
            if (pos.x < -padding || pos.x > windowWidth+padding ||
                pos.y < -padding || pos.y > windowHeight+padding) return false;
            return true;
        }

    /*
        Checks what countries are visible to improve performance and 
        not to be forced to render every country. It loops through each
        center point of a country, converts its position to pixels and
        then calculates the distances between it and 5 points of the screen
        to dertermine, weather it shall be displayed later on.

        Parameters:  baseCoordinates:    PVector    lat/long coordiantes at 0|0
                     coordinatePerPixel: double     similiar to zoom
    */
    this.getVisibleCountries = function(baseCoordinates, coordinatePerPixel) {
        // Check Interval
        if (Date.now() - this.lastPosCheck < this.lastPosCheckInterval) return this.currentCountries;
        this.lastPosCheck = Date.now();

        // Check each countriesCenter's distance to 5 points on the screen
        this.currentCountries = [];
        for (const c of this.countriesCenter) {
            const pixels = coordinatesToPixel(new Coordinate(c["long"], c["lat"]), baseCoordinates, coordinatePerPixel)
            const screenPositions = [ // 4 corners + screen center
                createVector(windowWidth/2, windowHeight/2),
                createVector(0, 0),
                createVector(windowWidth, windowHeight),
                createVector(0, windowHeight),
                createVector(windowWidth, 0)
            ];

            // Check distances, if at least one country is in reach it's added
            const maxDistance = 2*windowWidth; // px
            for (const pos of screenPositions) {
                const distance = Math.sqrt( Math.pow(pixels.x-pos.x, 2) + Math.pow(pixels.y-pos.y, 2) );
                if (distance <= maxDistance) {
                    if (!this.currentCountries.includes(c["country"])) { // mimics a set()
                        this.currentCountries.push( c["country"] );
                    }
                }
            }
            // print(c["country"], pixels.x, pixels.y, distance)
        }

        // Limit to a max amount of countries
        const maxCountries = 15;
        this.currentCountries = this.currentCountries.slice(0, maxCountries);

        print(this.currentCountries);
        return this.currentCountries;
    }
}