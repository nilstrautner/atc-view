/*
    This class Target holds all important information 
    after pulled via the api. 
*/
function Target (
        pos,                        
        callsign,
        type,
        altitude,
        altitude_ap,
        rate,
        speed,
        track,
        category
    ) {
        this.pos = pos;              // Coordinate 
        this.pixels = createVector(0, 0);
        this.callsign = callsign;
        this.type = type;
        this.altitude = altitude;
        this.altitude_ap = altitude_ap;
        this.rate = rate;
        this.speed = speed;
        this.track = track;
        this.category = category; // L, M, H, ?

        // check for undefineds
        if (this.callsign === undefined) this.callsign = "--"
        if (this.type === undefined) this.type = "--"
        if (this.altitude === undefined) this.altitude = "--"
        if (this.altitude_ap === undefined) this.altitude_ap = ""
        if (this.speed === undefined) this.speed = "--"
        if (this.rate === undefined) this.rate = 0
        if (this.track === undefined) this.track = -1

        this.lastUpdate = millis();

        // History Dots
        this.dots = [];             // Coordinate[]
        this.maxDots = 5;

        // Label
        this.labelOffsetPixel = 30;
        this.labelOffset = createVector(50, 50);
        this.labelSize = new Size(80, 55);
        this.dragging = false;
        this.clickOffset = createVector(0,0);
    
    /* 
        Displays and draws the current target with its
        head, line and label.
        Parameter:  baseCoordinates: PVector    lat/long coordiantes at 0|0
                    coordinatePerPixel: double  similiar to zoom
                    opacity: double             0.0-1.0 opacity of all visible elements
    */
    this.display = function(baseCoordinates, coordinatePerPixel, opacity) {
        // Skip if not visible
        if (!this.isVisible(baseCoordinates, coordinatePerPixel)) return;

        const pixels = coordinatesToPixel(this.pos, baseCoordinates, coordinatePerPixel);
        this.pixels = pixels;

        // Gray out when on ground
        if (this.speed < 40) {
            opacity = 0.3;
        }
        
        // History Dots
        if (checkbox_historydots.checked()) {
            let c = colorpicker_label.color();
            c.setAlpha(opacity*255);
            fill(c);
            noStroke();
            this.dots.forEach( (dot, i) => {
                //if (i == this.dots.length-1 && this.dots.length != 1) return;
    
                const p = coordinatesToPixel(dot, baseCoordinates, coordinatePerPixel);
                ellipse(p.x, p.y, 3, 3);
            });
        }

        
        // Label Position/Properties
        const textHeight = 12;
        textSize(textHeight);
        textAlign(LEFT);
        let heightOffset = 2;
        
        // Label content
        if (checkbox_label.checked()) {
            let line1 = this.callsign + "" + this.type;
            if (this.category === 'H') line1 += "  " + this.category;
            this.labelSize.width = textWidth(line1);
            this.labelSize.height = 3 * textHeight;
            
            // Label dragging
            if (this.dragging) {
                this.labelOffset.x = mouseX - pixels.x + this.clickOffset.x;
                this.labelOffset.y = mouseY - pixels.y + this.clickOffset.y;
            }
            
            const labelPos = createVector(
                pixels.x + this.labelOffset.x,
                pixels.y + this.labelOffset.y
            )
            
            noStroke();
            let c = colorpicker_label.color();
            c.setAlpha(opacity*255);
            fill(c);
            text(line1, labelPos.x, labelPos.y + textHeight);
            
            let prefix = "A";
            let sliceMax = 3;
            if (this.altitude < 10000) sliceMax = 2;
            if (this.altitude < 7000) sliceMax = 4;
            if (this.altitude > 7000) {
                prefix = "FL";
            }
            let altitude_string = String(Math.round(this.altitude/100)*100).slice(0, sliceMax);
            if (isNaN(altitude_string)) altitude_string = "--"
            
            let rate_string = "";
            this.rate_r = Math.round(this.rate/100)*100;
            
            if (this.rate_r > 100) {
                rate_string =  String.fromCharCode(0x2191) + Math.abs(this.rate_r);
            } else if (this.rate < -100) {
                rate_string =  String.fromCharCode(0x2193) + Math.abs(this.rate_r);
            } 

            //text(prefix + String(this.altitude).slice(0, sliceMax), labelPos.x, labelPos.y + 2*textHeight);
            text(prefix+altitude_string + "  " + rate_string, labelPos.x, labelPos.y + 2*textHeight);
            //text(prefix + String(this.altitude).slice(0, sliceMax) + "  " + this.altitude_ap, labelPos.x, labelPos.y + 2*textHeight);
            
            // Speed
            let speedOutput = Math.round(this.speed/10);
            if (isNaN(speedOutput)) speedOutput = "--";
            text(speedOutput, labelPos.x, labelPos.y + 3 * textHeight);
            
            // Last update
            c = colorpicker_label.color();
            c.setAlpha(opacity*255);
            fill(c);
            const cross = String.fromCharCode(0x2020);
            if (millis()-this.lastUpdate > 6 * 1000) {
                const timeDif = ((millis()-this.lastUpdate) / 1000).toFixed(0);
                text(cross + " " + timeDif + "s", labelPos.x, labelPos.y + 4 * textHeight + heightOffset);
                this.labelSize.height = 4.2 * textHeight;
            } else {
                this.labelSize.height = 3 * textHeight;
            }

            // Line
            const edges = [ createVector(labelPos.x, labelPos.y),
                            createVector(labelPos.x+this.labelSize.width, labelPos.y),
                            createVector(labelPos.x, labelPos.y+this.labelSize.height),
                            createVector(labelPos.x+this.labelSize.width, labelPos.y+this.labelSize.height) ];
            let labelEdge = edges[0];
            edges.forEach( (edge) => {
                if (edge.dist(pixels) < labelEdge.dist(pixels)) labelEdge = edge;
            });

            c = colorpicker_label.color();
            c.setAlpha(opacity*255);
            stroke(c);
            noFill();
            const ll = createVector(labelEdge.x-pixels.x, labelEdge.y-pixels.y).mult(0.9);
            const linestart = createVector(pixels.x + (ll.x*0.08), pixels.y + (ll.y*0.08));
            line(linestart.x, linestart.y, pixels.x+ll.x, pixels.y+ll.y);
        }

        // Future Vector
        if (checkbox_futurevector.checked()) {
            if (this.track != -1 && this.track != undefined && this.track != null) {

                c = colorpicker_label.color();
                c.setAlpha(opacity*255);
                stroke(c);
                const difLat = Math.cos(this.track*(Math.PI / 180))*(this.speed/60);
                const difLong = Math.sin(this.track*(Math.PI / 180))*(this.speed/60);
                const LAT_SCALE_FACTOR = 1/Math.cos(baseCoordinates.lat * (Math.PI / 180)); // mercator fix
                const futureCoordinate = new Coordinate(
                    this.pos.lat + difLat/1852*30,
                    this.pos.long + difLong/1852*30*LAT_SCALE_FACTOR
                );
                const futurePixels = coordinatesToPixel(futureCoordinate, baseCoordinates, coordinatePerPixel);
                if (!(futurePixels.x == 0 && futurePixels.y == 0) )
                    line(pixels.x, pixels.y, futurePixels.x, futurePixels.y);
            } else {
                // print("future vector problem with", this.callsign)
            }
        }
        
        // Head
        c = colorpicker_background.color();
        c.setAlpha(opacity*255);
        fill(c);
        c = colorpicker_label.color();
        c.setAlpha(opacity*255);
        stroke(c);
        rectMode(CENTER);
        rect(pixels.x, pixels.y, 8, 8)
    }
    
    /* 
        If this target didn't get an update for 
        a specific amount of seconds, it is too old
        and can be removed.
        Returns: State if removable
    */
    this.isDead = function() {
        const DURATION = 20 * 1000; // in ms
        if ((millis() - this.lastUpdate) > DURATION) return true;
        return false;
    }

    /*
        Updates information based on its own, new target information.

        Parameters:   newTarget: Target     new target data
    */
    this.update = function(newTarget) {
        // When no movement
        if (this.pos.lat === newTarget.pos.lat 
            && this.pos.long === newTarget.pos.long) 
            return;

        // History dots
        this.dots.push(structuredClone(this.pos));
        if (this.dots.length > this.maxDots) {
            this.dots.shift();
        }
        
        // Update properties
        this.pos = newTarget.pos;
        this.altitude = newTarget.altitude;
        this.speed = newTarget.speed;
        this.track = newTarget.track;
        if (this.category === '?') this.category = newTarget.category
        this.lastUpdate = millis();
    }

    /*
        Checks if the target with a puffer is theoretically
        located inside the screen.
        
        Returns:    visible: bool
    */
    this.isVisible = function(baseCoordinates, coordinatePerPixel) {
        const pixels = coordinatesToPixel(this.pos, baseCoordinates, coordinatePerPixel);
        const radius = 100 // in px

        if ( pixels.x < 0-radius ||
            pixels.x > windowWidth+radius ||
            pixels.y < 0-radius || 
            pixels.y > windowHeight+radius ) {
                return false;
            }
        return true;
    }

    /*
        Checks if a label is clicked for dragging.
        Starts dragging action.

        Parameter:  mousePos: PVector
        Returns:    success: boolean
    */
    this.clickLabel = function(mousePos) {
        const labelPos = createVector(
            this.pixels.x + this.labelOffset.x,
            this.pixels.y + this.labelOffset.y
        )
        if (mousePos.x < labelPos.x || mousePos.x > labelPos.x + this.labelSize.width ||
            mousePos.y < labelPos.y || mousePos.y > labelPos.y + this.labelSize.height) 
                return;

        this.dragging = true;
        this.clickOffset = createVector(
            this.pixels.x + this.labelOffset.x - mousePos.x,
            this.pixels.y + this.labelOffset.y - mousePos.y
        )

        return true;
    }
}