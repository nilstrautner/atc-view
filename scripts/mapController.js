/*
    This class controls everything that has to do with
    the map.

    Note:   Last used user map position and zoom is stored and loaded in localStorage
            when cookies have been accepted.
*/
function MapController (
    baseCoordinates     // lat, long reference at (x: 0, y: 0)
) {
    this.baseCoordinates = baseCoordinates; // Coordinate
    this.coordinatePerPixel = 0.00062490;     // similiar to zoom

    this.moveMap = false;
    this.offsetDrag = null
    this.baseCoordinatesBeforeDrag = null

    // Cookie position save
    this.lastPositionSave = Date.now();
    this.savePosition = false;

    /* 
        Displays/Draws the background of the map.
    */
    this.display = function() {
        background(colorpicker_background.color());
    }

    /*
        If the map is about to be moved, it changes the coordinate
        system accordingly.
    */
    this.checkMoveMap = function() {
        if (!this.moveMap) return;
        const LAT_SCALE_FACTOR = 1/Math.cos(baseCoordinates.lat * (Math.PI / 180)); // mercator fix
        
        const pixelDif = createVector(mouseX, mouseY).sub(this.offsetDrag)
        this.baseCoordinates.long = this.baseCoordinatesBeforeDrag.long - (pixelDif.x * this.coordinatePerPixel)
        this.baseCoordinates.lat = this.baseCoordinatesBeforeDrag.lat + (pixelDif.y * this.coordinatePerPixel) / LAT_SCALE_FACTOR

        this.checkSavePosition();
    }

    /*
        If the map is about to be zoomed, it changes the coordinate
        system accordingly.
        Parameters: zoom delta (int), about +/- 100
    */
   this.zoom = function(delta) {
        const zoomSpeed = 0.0001;
        
        const mouseCordsBeforeZoom = pixelToCoordinates(createVector(mouseX, mouseY), this.baseCoordinates, this.coordinatePerPixel);
        this.coordinatePerPixel += (delta/100) * zoomSpeed
        if (this.coordinatePerPixel < 0.0001249) this.coordinatePerPixel = 0.0001249; // max zoom in
        if (this.coordinatePerPixel > 0.00712490) this.coordinatePerPixel = 0.00712490; // max zoom out
        
        
        
        const mouseCordsAfterZoom = pixelToCoordinates(createVector(mouseX, mouseY), this.baseCoordinates, this.coordinatePerPixel);
        this.baseCoordinates.long -= mouseCordsAfterZoom.long - mouseCordsBeforeZoom.long;
        this.baseCoordinates.lat -= mouseCordsAfterZoom.lat - mouseCordsBeforeZoom.lat;

        this.checkSavePosition();
   }
    
    /*
        Calculates the center coordinates of the screen.
        Returns:    coordinates: Coordinate     lat/long of center point
    */
    this.getCenter = function() {
        return pixelToCoordinates(
            createVector(windowWidth/2, windowHeight/2),
            mapController.baseCoordinates,
            mapController.coordinatePerPixel
        );
    }

    /*
        Sets the center of the screen to a coordinate.

        Parameter: pos: Coordinate  desired position to be the middle on
    */
    this.setCenter = function(pos) {
        if (pos === undefined) return undefined;
        print("setting", pos)

        const LAT_SCALE_FACTOR = 1/Math.cos(baseCoordinates.lat * (Math.PI / 180)); // mercator fix
        this.baseCoordinates.lat = pos.lat + (windowHeight/2 * this.coordinatePerPixel)/LAT_SCALE_FACTOR;
        this.baseCoordinates.long = pos.long - (windowWidth/2 * this.coordinatePerPixel);
    }

    /*
        Is called when the map is being moved. It checks whether the interval
        is ready to save the current position (baseCoordinates) to the local storage.
    */
        this.checkSavePosition = function() {
            const dif = 1000; 
            if (this.lastPositionSave + dif > Date.now()) return;
            
            this.lastPositionSave = Date.now();
            localStorage.setItem("lat", this.baseCoordinates.lat);
            localStorage.setItem("long", this.baseCoordinates.long);
            localStorage.setItem("coordinatePerPixel", this.coordinatePerPixel);
            console.log("MapController: Map position saved")
        }

    /*
        Checks if the localstorage properties are set correctly and if so,
        loads the elements and positions the map and zoom to the users 
        last saved preferences.
    */
        this.loadSavedPosition = function() {
            if (!this.savePosition) return;

            // Check if null
            if (localStorage.getItem("lat") == undefined ||
                localStorage.getItem("long") == undefined || 
                localStorage.getItem("coordinatePerPixel") == undefined) {
                    console.log("MapController: No saved position found");
                    return;
                }
            
            // Load
            this.baseCoordinates = new Coordinate(
                parseFloat(localStorage.getItem("lat")),
                parseFloat(localStorage.getItem("long"))
                );
            this.coordinatePerPixel = parseFloat(localStorage.getItem("coordinatePerPixel"));
            console.log("MapController: Loaded user position")
        }
}

/*
    Global Method that converts pixel to the coordinate system.
    Parameters:     posPixel: PVector           pixel on screen
                    baseCoordinates: PVector    lat/long coordiantes at 0|0
                    coordinatePerPixel: double  similiar to zoom
    Returns:        coordinates: Coordinate        lat/long coordinates at point
*/
function pixelToCoordinates(posPixel, baseCoordinates, coordinatePerPixel) {
    const LAT_SCALE_FACTOR = 1/Math.cos(baseCoordinates.lat * (Math.PI / 180)); // mercator fix

    const difCordLong = posPixel.x * coordinatePerPixel;
    const difCordLat = posPixel.y * coordinatePerPixel / LAT_SCALE_FACTOR;

    return new Coordinate(
        baseCoordinates.lat - difCordLat,
        baseCoordinates.long + difCordLong
    );
}

/*
    Global Method that converts pixel to the coordinate system.
    Parameters:     posCords: PVector           lat/long coordinates at point
                    baseCoordinates: Coordinate    lat/long coordiantes at 0|0
                    coordinatePerPixel: double  similiar to zoom
    Returns:        posPixel: PVector           pixel on screen
*/
function coordinatesToPixel(posCords, baseCoordinates, coordinatePerPixel) {
    const LAT_SCALE_FACTOR = 1/Math.cos(baseCoordinates.lat * (Math.PI / 180)); // mercator fix

    return createVector(
        - (baseCoordinates.long - posCords.long) / coordinatePerPixel,
        (baseCoordinates.lat - posCords.lat) / coordinatePerPixel * LAT_SCALE_FACTOR 
    );
}

