/*
    This class handles the weather information
    displayed in the top left corner via API GET 
    requests.
*/
function WeatherService ( ) {
    this.currentAirport = undefined;
    this.currentMetar = "No METAR";

    this.lastGet = 0; // last api get

    /*
        Updates the current metar.
        Parameter: airport: string  Metar of desired METAR
    */
    this.update = function(airport) {
        if (airport === undefined || airport === null) return;

        if (airport === this.currentAirport) {
            if (millis() - this.lastGet > 600000) { // > 10 min
                // TODO update current
            } else {
                return;
            }
        }
        
        this.lastGet = millis();
        this.currentAirport = airport;
        this.apiGetMetar();
    }   

    /*
        Makes an asynch API GET request and writes the 
        latest METAR to this.currentMetar
    */
    this.apiGetMetar = async function() {
        try {
          let url = "https://api.met.no/weatherapi/tafmetar/1.0/metar.txt?icao=" + this.currentAirport;
          httpGet(url, 'text', false, (response) => {
            const response_split = response.split("\n");
            this.currentMetar = response_split[response_split.length - 3].trim();
            console.log(this.currentMetar);
          });
        } catch (error) {
          console.error("Error when fetching METAR via api call", error);
        }
      }

    /*
        Displays the weather in the top left corner.
    */
    this.display = function() {
        if (checkbox_weather.checked()) {
            const textHeight = 12;
            textSize(textHeight);
            textAlign(LEFT);
            noStroke();
            fill(colorpicker_weather.color()); 
    
            text(this.currentMetar, 4, textHeight+4);
        }
    }

    /*
        Checks if another api get call is
        in the interval to be made.
        Return: isReady: boolean
    */
    this.isReady = function() {
        const DURATION = 2 * 1000; // in ms
        if ((millis() - this.lastGet) > DURATION) return true;
        return false;
    }

}