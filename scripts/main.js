/*
  This is the main class from which all logic is controlled.
  It is the entry point for p5.js
*/

// Instance variables
let mapController;
let apiController;
let targetController;
let airportController;
let weatherService;
let searchBar;
let debug;
let settings;
let distanceController;
let mapLines;

let liveTraffic = false; // is set by checkbox or localStorage

let debugMode = true;

/*
  This is the setup method for p5.js
  It generates the canvas, creates all important 
  objects, loads the airspaces and makes an initial
  api call to load traffic.
*/
function setup() {
  createCanvas(windowWidth, windowHeight+3*16);
  
  // Controllers
  mapController = new MapController(new Coordinate(50.2, 8.1));
  apiController = new ApiController(mapController.baseCoordinates, 50);
  airportController = new AirportController();
  targetController = new TargetController();
  weatherService = new WeatherService();
  distanceController = new DistanceController();
  searchBar = new Searchbar();
  debug = new Debug();
  settings = new Settings();

  mapLines = new MapLines();
  mapLines.loadCountries();
  mapLines.loadAirspaces();

  settings.init();
}

/*
  This is the main loop.
  It updates all data and renders the screen.
*/
function draw() {
  liveTraffic = checkbox_live.checked();
  if (!liveTraffic) targetController.deleteAll();

  // update
  if (liveTraffic) {
    if (!apiController.dataPulled) targetController.feedData(apiController.pullData());
    apiController.fetchData(
      mapController.getCenter(), 
      mapController.coordinatePerPixel*50000
      );
    } else {
      // ONLY DEBUG?
      // targetController.feedData(debug.pullData());
    }
  mapController.checkMoveMap()
  distanceController.update(targetController.targets)
  

  // update weather
  if (mapController.moveMap && weatherService.isReady() ||
      weatherService.currentAirport === undefined) {
    const ap = airportController.getNearestAirport(
      createVector(windowWidth/2, windowHeight/2), 
        mapController.baseCoordinates, 
        mapController.coordinatePerPixel);
    weatherService.update(ap);
  }

  // update search
  if (searchBar.isNewSearch()) {
    // search for airport
    searchBar.searchAirport();

    // search for callsign
    const callsign = searchBar.searchCallsign();
    apiController.fetchDataSingle(callsign);
  }

  if (!apiController.searchTargetUsed) {
    mapController.setCenter(apiController.getSearchTargetPosition());
    targetController.feedData([apiController.searchTarget]);
  }
  if (!searchBar.airportHandled) {
    searchBar.airportHandled = true;
    mapController.setCenter(searchBar.airportCoords);
  }
  
  // display
  mapController.display();
  mapLines.displayCountries(mapController.baseCoordinates, mapController.coordinatePerPixel);
  mapLines.displayAirspaces(mapController.baseCoordinates, mapController.coordinatePerPixel);
  airportController.displayAll(mapController.baseCoordinates, mapController.coordinatePerPixel);

  targetController.displayAll(mapController.baseCoordinates, mapController.coordinatePerPixel);
  weatherService.display();
  distanceController.display(mapController.baseCoordinates, mapController.coordinatePerPixel);

  // debug
  if (debugMode) debug.displayDebugCoordinates(targetController.targets.size);

  // settings
  if (settings.open) settings.display();
}

// This method is called when the window is resized.
function windowResized() {
  resizeCanvas(windowWidth, windowHeight);
}

// This method is called once when the mouse is pressed.
function mousePressed() {
  // if (mouseButton === LEFT) { // Doenst work on phone then
    // Target click
    const targetClicked = targetController.click(
      createVector(mouseX, mouseY), 
      mapController.baseCoordinates, 
      mapController.coordinatePerPixel
    );

    // Map move
    if (!targetClicked) {
      if (!settings.open || (settings.open && mouseX > settings.width)) { // ignore sides because controls
        mapController.moveMap = true;
        mapController.offsetDrag = createVector(mouseX, mouseY);
        mapController.baseCoordinatesBeforeDrag = structuredClone(mapController.baseCoordinates);
      }
    }
  // }
}

// This method is called once when the mouse is released.
function mouseReleased() {
  mapController.moveMap = false;
  targetController.stopDragging();
}

// This method is called once when the mouse wheel is rotated.
function mouseWheel(event) {
  mapController.zoom(event.delta);
}

// This method is called when double click on canvas is triggered.
function doubleClicked() {
  // Distance Double Click
  target = targetController.getTargetByMousePosition(
    createVector(mouseX, mouseY), 
    mapController.baseCoordinates, 
    mapController.coordinatePerPixel);
  distanceController.add(target, mapController.baseCoordinates, mapController.coordinatePerPixel);
}