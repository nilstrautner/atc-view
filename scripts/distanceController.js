/*
    This class handles the weather information
    displayed in the top left corner via API GET 
    requests.
*/
function DistanceController ( ) {
    
    this.pairs = [];

    this.currentMeasureTarget = null;
    this.measuring = false;

    this.targets = new Map(); // local copy 

    /*
        This method is called when a double click is performed and the main
        method has identified a click on a target head. It logs a first click
        and finalises and adds a new complete measurement after a second head
        is clicked.

        Parameters: target:  Target   the target of a click
    */
    this.add = function(target, baseCoordinates, coordinatePerPixel) {
        // Double Click nowhere
        if (target == null) {
            // Stationary measurement at mouse pos
            if (this.measuring) {
                const tmp = new Target(pixelToCoordinates(createVector(mouseX, mouseY), baseCoordinates, coordinatePerPixel), "MOUSECLICK");
                tmp.pixels = createVector(mouseX, mouseY);
                this.pairs.push( [this.currentMeasureTarget, tmp] );
                this.measuring = false;
                this.currentMeasureTarget = null;
                return;
            }

            // Otherwise it is a delete operation
            else {
                this.measuring = false;
                this.currentMeasureTarget = null;
                this.removeMeasurement();
                return;
            }
        }

        // First one
        if (!this.measuring) {
            this.measuring = true;
            this.currentMeasureTarget = target;
            return;
        }
        
        // Second one
        if (target.callsign != this.currentMeasureTarget.callsign)
            this.pairs.push( [this.currentMeasureTarget, target] );
        print("Adding Distance Measure Line between", this.currentMeasureTarget.callsign, "and", target.callsign)
        this.measuring = false;
        this.currentMeasureTarget = null;
    }   

    /*
        This function is called when a double click on the map is performed.
        It checks if the mouse position is inside a measurement rectangle and
        removes it. Uses p5js internal mouseX and mouseY properties.
    */
    this.removeMeasurement = function() {
        for (let i = 0; i < this.pairs.length; i++) {
            
            // Calculate pos of text box
            let target1 = this.targets.get(this.pairs[i][0].callsign);
            let target2 = this.targets.get(this.pairs[i][1].callsign);

            // When one of them is a stationary mouseclick
            if (target1 == undefined && this.pairs[i][0].callsign === "MOUSECLICK") target1 = this.pairs[i][0];
            if (target2 == undefined && this.pairs[i][1].callsign === "MOUSECLICK") target2 = this.pairs[i][1];

            const middleX = target1.pixels.x + (target2.pixels.x - target1.pixels.x) / 2;
            const middleY = target1.pixels.y + (target2.pixels.y - target1.pixels.y) / 2;
            const w = 55;
            const h = 18;
            rect(middleX, middleY, 55, 18);
            
            // Remove if click is inside it
            if (mouseX > middleX-w/2 && 
                mouseX < middleX+w/2 &&
                mouseY > middleY-h/2 &&
                mouseY < middleY+h/2) {
                    this.pairs.splice(i, 1);
                    return;
                }   
        }
            
    }


    /*
        Makes a local copy of all targets from mapController to
        this class, so updated target head positions can be used
        for a moving measurement line.

        Parameters: targets: Map()  local targets copy
    */
    this.update = function(targets) {
        this.targets = targets;
    }   

   
    /* 
        Displays all known targets depending on the selected
        altitude filter. If a target is dead, it will be deleted.

        Parameters: baseCoordinates: PVector    lat/long coordiantes at 0|0
                    coordinatePerPixel: double  similiar to zoom
    */
    this.display = function(baseCoordinates, coordinatePerPixel) {
        let toDelete = []
        for (let i = 0; i < this.pairs.length; i++) {
            let target1 = this.targets.get(this.pairs[i][0].callsign);
            let target2 = this.targets.get(this.pairs[i][1].callsign);

            // When one of them is a stationary mouseclick
            if (target1 == undefined && this.pairs[i][0].callsign === "MOUSECLICK") {
                target1 = this.pairs[i][0];
                target1.pixels = coordinatesToPixel(target1.pos, baseCoordinates, coordinatePerPixel);
            }
            if (target2 == undefined && this.pairs[i][1].callsign === "MOUSECLICK") {
                target2 = this.pairs[i][1];
                target2.pixels = coordinatesToPixel(target2.pos, baseCoordinates, coordinatePerPixel);
            }
            
            // If one of them is dead
            if (target1 == undefined || target2 == undefined) {
                toDelete.push(this.pairs[i]);
                continue;
            }

            // If both are not visible
            if (!target1.isVisible(baseCoordinates, coordinatePerPixel) ||
                !target2.isVisible(baseCoordinates, coordinatePerPixel))
                continue
            
            // Display Line
            stroke(252, 140, 3);
            line(target1.pixels.x, target1.pixels.y, target2.pixels.x, target2.pixels.y); 

            // End point if mouse click
            if (target2.callsign === "MOUSECLICK") {
                fill(252, 140, 3);
                ellipseMode(CENTER);
                ellipse(target2.pixels.x, target2.pixels.y, 5, 5);
            }

            // Calculate distance in nm
            let d = this.calculateDistance(target1.pos, target2.pos);
            d = d.toFixed(1);
            
            // Display Text Box
            const middleX = target1.pixels.x + (target2.pixels.x - target1.pixels.x) / 2;
            const middleY = target1.pixels.y + (target2.pixels.y - target1.pixels.y) / 2;
            const w = 55;
            const h = 18;
            fill(colorpicker_background.color());
            stroke(252, 140, 3);
            rectMode(CENTER);
            rect(middleX, middleY, w, h); 
            
            // Display Text
            fill(252, 140, 3);
            noStroke();
            textAlign(CENTER);
            text(d + " NM", middleX, middleY+4);
        }

        // Deleting pairs, where at least one plane is dead
        for (const pair of toDelete) {
            print("DELETING PAIR", pair)
            const index = this.pairs.indexOf(pair);
            this.pairs.splice(index, 1);
        }

        // Display measuring line
        if (this.measuring) {
            target = this.targets.get(this.currentMeasureTarget.callsign);
            
            if (target.isVisible(baseCoordinates, coordinatePerPixel)) {
                stroke(252, 140, 3);
                line(target.pixels.x, target.pixels.y, mouseX, mouseY); 
            } else {
                // Abort operation
                this.measuring = false;
                this.currentMeasureTarget = null;
            }

        }
    }

    /*
        Calculates the distances between two Coordinates and returns it
        distance in nautical miles (nm).

        Parameters: pos1:  Coordinate   position 1
                    pos2:  Coordinate   position 2
    */
    this.calculateDistance = function(pos1, pos2) {
        // Earth's radius in nautical miles = 3440.065
        var earthRadiusNm = 3440.065;
    
        // Convert degrees to radians
        var lat1Rad = pos1.lat * Math.PI / 180;
        var lat2Rad = pos2.lat * Math.PI / 180;
        var deltaLatRad = (pos2.lat - pos1.lat) * Math.PI / 180;
        var deltaLonRad = (pos2.long - pos1.long) * Math.PI / 180;
    
        // Haversine formula
        var a = Math.sin(deltaLatRad / 2) * Math.sin(deltaLatRad / 2) +
                Math.cos(lat1Rad) * Math.cos(lat2Rad) *
                Math.sin(deltaLonRad / 2) * Math.sin(deltaLonRad / 2);
        var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
    
        // Distance in nautical miles
        var distance = earthRadiusNm * c;
        return distance;
    }


}