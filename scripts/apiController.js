/*
    This class makes the actual api get requests to the
    adsb database service.
*/
function ApiController ( ) {
    // Time interval
    this.interval = 3000 // in ms

    this.URLs = [
        "https://api.adsb.one/v2/",
        "https://api.adsb.lol/v2/" // CORS problem
    ]

    this.data = [];
    this.dataPulled = true;

    // Single call
    this.searchTarget = undefined;
    this.lastGetSearchTarget = millis();
    this.searchTargetUsed = true;

    /*
        This method makes a api get request to the adsb api
        to get all traffic inside a specific radius when the
        last request is below the interval.

        Parameters: cords (Coordinate): Center of screen 
                    from where to request its radius.
                    range (int): range in nm to get traffic
    */
    this.fetchData = function(cords, range) {
        try {
            // Check time
            if (!this.newUpdate()) return;
            this.lastGet = millis(); 
    
            // API GET CALL
            let url =  this.URLs[0] + "point/" + cords.lat + "/" + cords.long + "/" + range;
            httpGet(url, 'json', false, function(response) {
                let ac = response["ac"];
                print(ac.length + " aircrafts detected");
                
                // Paste to targets
                this.data = [];
                ac.forEach( (a) => {
                    let altitude = a.alt_baro;
                    if (isNaN(altitude) && 
                        altitude !== undefined && 
                        altitude.includes("ground")) 
                            altitude = 0;
                    
                    let category = '?';
                    if (a.category === 'A1') category = 'L'
                    if (a.category === 'A2') category = 'M'
                    if (a.category === 'A3') category = 'M'
                    if (a.category === 'A4') category = 'H'
                    if (a.category === 'A5') category = 'H'
                    this.data.push(new Target(
                        new Coordinate(a.lat, a.lon),
                        a.flight,
                        a.t,
                        altitude,
                        a.nav_altitude_mcp,
                        a.baro_rate,
                        a.gs,
                        a.track,
                        category
                        ));
                    });

                    this.dataPulled = false;
                }.bind(this));
        } catch (error) {
            console.error("Error when fetching many targets", error);
        }
    }

    this.pullData = function() {
        if (this.data.length > 0) {
            this.dataPulled = true;
            return this.data;
        } 

        return [];
    }

    this.newUpdate = function() {
        if (millis() < this.interval+this.lastGet ) return false
        else return true
    }


    this.fetchDataSingle = function(callsign) {
        try {
            if (callsign === undefined) return; 
            if (millis() < this.lastGetSearchTarget+3000) return;
    
            // API GET CALL
            let url =  this.URLs[0] + "callsign/" + callsign;
            httpGet(url, 'json', false, function(response) {
                console.log("made api call");
                let ac = response["ac"];
                print(ac.length + " aircrafts detected");
                
                // Paste to target
                const a = ac[0];
                if (a === undefined) return;

                let altitude = a.alt_baro;
                    if (isNaN(altitude) && 
                        altitude !== undefined && 
                        altitude.includes("ground")) 
                            altitude = 0;
                
                this.searchTarget = new Target(
                    new Coordinate(a.lat, a.lon),
                    a.flight,
                    a.t,
                    altitude,
                    a.nav_altitude_mcp,
                    a.baro_rate,
                    a.gs,
                    a.track
                    );
    
                    this.searchTargetUsed = false;
                    this.lastGetSearchTarget = millis();
                    this.lastGet = millis(); // avoiding too many requests
                }.bind(this));
        } catch (error) {
            console.error("Error when fetching single data", error)
        }
            
    }

    this.getSearchTargetPosition = function() {
        if (this.searchTargetUsed || this.searchTarget === undefined) 
            return undefined;
        
        this.searchTargetUsed = true;
        return this.searchTarget.pos;
    }


}