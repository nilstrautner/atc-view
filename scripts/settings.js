// Public availble colors
let colorpicker_background;
let colorpicker_label;
let colorpicker_airspace;
let colorpicker_border;
let colorpicker_weather;

let checkbox_live;
let checkbox_label;
let checkbox_futurevector;
let checkbox_historydots;
let checkbox_airportmarker;
let checkbox_weather;
let checkbox_search;

/*
    Settings class. Holds information for settings regarding gui and co as well
    as rendering of the settings tab.

    Note: Settings are stored in cookies, when accepted. Saved, when menu is being closed.
*/
function Settings() {   
    this.open = false;

    this.width = 250;
    this.buttonClose = undefined;
    this.buttonOpen = undefined;
    this.buttonRevert = undefined;

    this.allowCookies = false;

    // Values from cookies
    this.cookie_colorpicker_background = '#323232';
    this.cookie_colorpicker_label = '#ffffff';
    this.cookie_colorpicker_airspace = '#878787';
    this.cookie_colorpicker_border = '#000000';
    this.cookie_colorpicker_weather = '#c7c7c7';

    this.default_colorpicker_background = '#323232';
    this.default_colorpicker_label = '#ffffff';
    this.default_colorpicker_airspace = '#878787';
    this.default_colorpicker_border = '#000000';
    this.default_colorpicker_weather = '#c7c7c7';

    this.cookie_checkbox_live = true;
    this.cookie_checkbox_label = true;
    this.cookie_checkbox_futurevector = true;
    this.cookie_checkbox_historydots = true;
    this.cookie_checkbox_airportmarker = true;
    this.cookie_checkbox_weather = true;
    this.cookie_checkbox_search = true;

    this.tSize = 18;
    /*
        Creates all color pickers and checkboxes directly on the dom.
    */
    this.init = function() {
        // Get values from cookies
        this.loadCookies();

        // Color pickers
        const paddingTop = 15;
        colorpicker_background = createColorPicker(this.cookie_colorpicker_background);
        colorpicker_background.position(10, 3*this.tSize-3);
        colorpicker_background.size(30, 30);
        colorpicker_background.style("border", "0");
        colorpicker_background.style("cursor", "pointer");

        colorpicker_label = createColorPicker(this.cookie_colorpicker_label);
        colorpicker_label.position(10, 4*this.tSize+1*paddingTop-3);
        colorpicker_label.size(30, 30);
        colorpicker_label.style("border", "0")
        colorpicker_label.style("cursor", "pointer");

        colorpicker_airspace = createColorPicker(this.cookie_colorpicker_airspace);
        colorpicker_airspace.position(10, 5*this.tSize+2*paddingTop-3);
        colorpicker_airspace.size(30, 30);
        colorpicker_airspace.style("border", "0");
        colorpicker_airspace.style("cursor", "pointer");

        colorpicker_border = createColorPicker(this.cookie_colorpicker_border);
        colorpicker_border.position(10, 6*this.tSize+3*paddingTop-3);
        colorpicker_border.size(30, 30);
        colorpicker_border.style("border", "0");
        colorpicker_border.style("cursor", "pointer");

        colorpicker_weather = createColorPicker(this.cookie_colorpicker_weather);
        colorpicker_weather.position(10, 7*this.tSize+4*paddingTop-3);
        colorpicker_weather.size(30, 30);
        colorpicker_weather.style("border", "0")
        colorpicker_weather.style("cursor", "pointer");

        // Checkbox
        checkbox_live =         createCheckbox("", this.cookie_checkbox_live); 
        checkbox_label =         createCheckbox("", this.cookie_checkbox_label); 
        checkbox_futurevector =  createCheckbox("", this.cookie_checkbox_futurevector); 
        checkbox_historydots  =  createCheckbox("", this.cookie_checkbox_historydots); 
        checkbox_airportmarker = createCheckbox("", this.cookie_checkbox_airportmarker); 
        checkbox_weather =       createCheckbox("", this.cookie_checkbox_weather); 
        checkbox_search =        createCheckbox("", this.cookie_checkbox_search); 
        
        const refY = 14*this.tSize+3*paddingTop;
        checkbox_live.position(15, refY+0*this.tSize+0*paddingTop-15) 
        checkbox_label.position(15, refY+1*this.tSize+1*paddingTop-15) 
        checkbox_futurevector.position(15, refY+2*this.tSize+2*paddingTop-15) 
        checkbox_historydots.position(15, refY+3*this.tSize+3*paddingTop-15) 
        checkbox_airportmarker.position(15, refY+4*this.tSize+4*paddingTop-15) 
        checkbox_weather.position(15, refY+5*this.tSize+5*paddingTop-15) 
        checkbox_search.position(15, refY+6*this.tSize+6*paddingTop-15) 
        
        checkbox_search.changed(() => {
            const s = document.getElementsByClassName("searchbar-wrapper");
            if (s == undefined) return;
            if (checkbox_search.checked()) s[0].style.display = "block";
            else s[0].style.display = "none";
        });

        // Default settings button
        this.buttonRevert = createButton('Set Default Settings');
        this.buttonRevert.position(15, 10*this.tSize+3*paddingTop);
        this.buttonRevert.style("border", "1px solid gray");
        this.buttonRevert.style("fontSize", "17px");
        this.buttonRevert.style("cursor", "pointer");
        this.buttonRevert.style("background", "transparent");
        this.buttonRevert.style("color", "black");
        this.buttonRevert.mousePressed(() => {
            this.cookie_colorpicker_background = this.default_colorpicker_background;
            this.cookie_colorpicker_label = this.default_colorpicker_label;
            this.cookie_colorpicker_airspace = this.default_colorpicker_airspace;
            this.cookie_colorpicker_border = this.default_colorpicker_border;
            this.cookie_colorpicker_weather = this.default_colorpicker_weather;
            colorpicker_background = createColorPicker(this.cookie_colorpicker_background);
            colorpicker_label = createColorPicker(this.cookie_colorpicker_label);
            colorpicker_airspace = createColorPicker(this.cookie_colorpicker_airspace);
            colorpicker_border = createColorPicker(this.cookie_colorpicker_border);
            colorpicker_weather = createColorPicker(this.cookie_colorpicker_weather);
            this.saveCookies();
            location.reload();
        });

        // Close button
        this.buttonClose = createButton('X');
        this.buttonClose.position(this.width-35, 5);
        this.buttonClose.style("border", "0");
        this.buttonClose.style("fontSize", "25px");
        this.buttonClose.style("cursor", "pointer");
        this.buttonClose.mousePressed(() => {
            this.open = false;
            this.hideElements();
            this.saveCookies();
        });

        // Open button
        this.buttonOpen = createButton('Settings');
        this.buttonOpen.position(5, windowHeight-30);
        this.buttonOpen.style("border", "0");
        this.buttonOpen.style("fontSize", "17px");
        this.buttonOpen.style("cursor", "pointer");
        this.buttonOpen.style("background", "transparent");
        this.buttonOpen.style("color", colorpicker_weather.color());
        this.buttonOpen.mousePressed(() => {
            this.open = true;
            this.showElements();
        });

        if (this.open) this.showElements();
        else this.hideElements();

    }   

    // Hides all UI elements
    this.hideElements = function() {
        colorpicker_weather.hide();
        colorpicker_airspace.hide();
        colorpicker_border.hide();
        colorpicker_label.hide();
        colorpicker_background.hide();

        checkbox_live.hide();
        checkbox_label.hide();
        checkbox_futurevector.hide();
        checkbox_historydots.hide();
        checkbox_airportmarker.hide();
        checkbox_weather.hide();  
        checkbox_search.hide();

        this.buttonRevert.hide();
        this.buttonClose.hide();
        this.buttonOpen.show();

        const s = document.getElementsByClassName("range-wrapper");
        if (s !== undefined) s[0].style.display = "none";
    }

    // Displays all UI elements
    this.showElements = function() {
        colorpicker_weather.show();
        colorpicker_airspace.show();
        colorpicker_border.show();
        colorpicker_label.show();
        colorpicker_background.show();

        checkbox_live.show();
        checkbox_label.show();
        checkbox_futurevector.show();
        checkbox_historydots.show();
        checkbox_airportmarker.show();
        checkbox_weather.show();  
        checkbox_search.show();

        this.buttonRevert.show();
        this.buttonClose.show();
        this.buttonOpen.hide();

        const s = document.getElementsByClassName("range-wrapper");
        if (s !== undefined) s[0].style.display = "block";
    }

    /*
        Displays the background, title, and all labels for the 
        settings input controls.
    */
    this.display = function() {
        // bg
        fill(255,255, 255, 230);
        rectMode(CORNER);
        rect(0, 0, this.width, windowHeight);  

        // properties
        noStroke();
        fill(10, 10, 10);
        textSize(this.tSize);
        textAlign(LEFT);

        textSize(1.2*this.tSize);
        text("Settings:", 10, 30);
        textSize(this.tSize);

        // Colors
        const paddingTop = 15;
        text("Background color", 50, 4*this.tSize) 
        text("Aircraft label color", 50, 5*this.tSize+1*paddingTop) 
        text("Airspace lines color", 50, 6*this.tSize+2*paddingTop) 
        text("Country border color", 50, 7*this.tSize+3*paddingTop) 
        text("Weather info color", 50,   8*this.tSize+4*paddingTop) 
        
        // Checkbox
        const refY = 14*this.tSize+3*paddingTop;
        text("Show live traffic", 50, refY+0*this.tSize+0*paddingTop) 
        text("Show aircraft label", 50, refY+1*this.tSize+1*paddingTop) 
        text("Show future vector", 50,  refY+2*this.tSize+2*paddingTop) 
        text("Show history dots", 50,   refY+3*this.tSize+3*paddingTop) 
        text("Show airport marker", 50, refY+4*this.tSize+4*paddingTop) 
        text("Show weather info", 50,   refY+5*this.tSize+5*paddingTop) 
        text("Show search bar", 50,     refY+6*this.tSize+6*paddingTop) 
    }
    
    // Saves all cookies
    this.saveCookies = function() {
        if (!this.saveCookies) return;

        this.setCookie("colorpicker_background", colorpicker_background.color().toString(), 30);
        this.setCookie("colorpicker_label", colorpicker_label.color().toString(), 30);
        this.setCookie("colorpicker_airspace", colorpicker_airspace.color().toString(), 30);
        this.setCookie("colorpicker_border", colorpicker_border.color().toString(), 30);
        this.setCookie("colorpicker_weather", colorpicker_weather.color().toString(), 30);

        this.setCookie("checkbox_live", checkbox_live.checked(), 30); 
        this.setCookie("checkbox_label", checkbox_label.checked(), 30); 
        this.setCookie("checkbox_futurevector", checkbox_futurevector.checked(), 30);
        this.setCookie("checkbox_historydots", checkbox_historydots.checked(), 30);
        this.setCookie("checkbox_airportmarker", checkbox_airportmarker.checked(), 30);
        this.setCookie("checkbox_weather", checkbox_weather.checked(), 30);
        this.setCookie("checkbox_search", checkbox_search.checked(), 30);
    }

    // Loads all cookies
    this.loadCookies = function() {
        if (!this.allowCookies) return; 

        if (this.getCookie("colorpicker_background") !== "") this.cookie_colorpicker_background = this.toColor(this.getCookie("colorpicker_background"));
        if (this.getCookie("colorpicker_label") !== "") this.cookie_colorpicker_label = this.toColor(this.getCookie("colorpicker_label"));
        if (this.getCookie("colorpicker_airspace") !== "") this.cookie_colorpicker_airspace = this.toColor(this.getCookie("colorpicker_airspace"));
        if (this.getCookie("colorpicker_border") !== "") this.cookie_colorpicker_border = this.toColor(this.getCookie("colorpicker_border"));
        if (this.getCookie("colorpicker_weather") !== "") this.cookie_colorpicker_weather = this.toColor(this.getCookie("colorpicker_weather"));

        if (this.getCookie("checkbox_live") !== "") this.cookie_checkbox_live = this.toBoolean(this.getCookie("checkbox_live"));
        if (this.getCookie("checkbox_label") !== "") this.cookie_checkbox_label = this.toBoolean(this.getCookie("checkbox_label"));
        if (this.getCookie("checkbox_futurevector") !== "") this.cookie_checkbox_futurevector = this.toBoolean(this.getCookie("checkbox_futurevector"));
        if (this.getCookie("checkbox_historydots") !== "") this.cookie_checkbox_historydots = this.toBoolean(this.getCookie("checkbox_historydots"));
        if (this.getCookie("checkbox_airportmarker") !== "") this.cookie_checkbox_airportmarker = this.toBoolean(this.getCookie("checkbox_airportmarker"));
        if (this.getCookie("checkbox_weather") !== "") this.cookie_checkbox_weather = this.toBoolean(this.getCookie("checkbox_weather"));
        if (this.getCookie("checkbox_search") !== "") this.cookie_checkbox_search = this.toBoolean(this.getCookie("checkbox_search"));
    }

    // This function creates and saves a new cookie.
    this.setCookie = function(cname, cvalue, exdays) {
        const d = new Date();
        d.setTime(d.getTime() + (exdays*24*60*60*1000));
        let expires = "expires="+ d.toUTCString();
        document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
      }

    // This function returns the value of a cookie.
    this.getCookie = function(cname) {
        let name = cname + "=";
        let decodedCookie = decodeURIComponent(document.cookie);
        let ca = decodedCookie.split(';');
        for(let i = 0; i <ca.length; i++) {
            let c = ca[i];
            while (c.charAt(0) == ' ') {
            c = c.substring(1);
            }
            if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
            }
        }
        return "";
    }

    // Converts the content of a string to a boolean object.
    this.toBoolean = function(text) {
        if (text === "true") return true;
        return false;
    }

    // Converts a string containing a color in rgba format to 
    // p5js Color object.
    this.toColor = function(text) {
        text = text.replace("rgba(", '');
        text = text.replace(')', '');
        return color(
            parseInt(text.split(',')[0]),
            parseInt(text.split(',')[1]),
            parseInt(text.split(',')[2]),
            parseInt(text.split(',')[3])
        );
    }

}
