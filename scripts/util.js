/*
    Coordinate class for correct usage.
*/
function Coordinate ( lat, long ) {
    this.lat = lat;
    this.long = long;
}

/*
    Size data format type with width and length in pixel.
*/
function Size ( width, height ) {
    this.width = width;
    this.height = height;
}

/*
    Checks if user device is a touch device.

    Returns:    state:  boolean     if its touch device
*/
function isTouchDevice() {
    return (('ontouchstart' in window) ||
        (navigator.maxTouchPoints > 0) ||
        (navigator.msMaxTouchPoints > 0));
}

/*
    Checks if a string contains a number.

    Parameter:  myString:   string    string to check
    Returns:    state:      boolean   contains numbers
*/
function hasNumber(myString) {
    return /\d/.test(myString);
}