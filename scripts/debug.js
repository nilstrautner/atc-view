/*
    This class is used for development to debug
    data and mock various scenarios.
*/
function Debug() {

    /*
        Mocks stationary traffic in the EDDF area.
        Parameter:  state   boolean     is it enabled?
    */
    this.pullData = function() {
        
        let data = [];
        data.push(new Target(new Coordinate(50.037889966899726, 8.931647732817309), "DLH123", "B737", 10000, 12000, 1000, 300, 45));
        data.push(new Target(new Coordinate(50.036239594290755, 8.560551582068332), "ABC501A", "A320", 5000, 12000, 1000, 300, 225));
        data.push(new Target(new Coordinate(49.86632805287813,  8.942094710033711), "CFG0123", "B777", 35000, 12000, -1000, 300, 135));
        data.push(new Target(new Coordinate(49.88768108491277,  8.234247063543195), "DETJA", "P28A", 12345, 12000, -1000, 300, 315));
        
        return data;
    }

    /*  
        This is a debug variable and should not be used
        in production. When state is enabled, it displays
        the current position of the mouse in pixels on screen
        and on coordinates in real world.

        totalTargets:   int  total catched targets
    */
    this.displayDebugCoordinates = function(totalTargets) {
        let mouse = createVector(mouseX, mouseY);
        let cord = pixelToCoordinates(
                            createVector(mouse.x, mouse.y), 
                            mapController.baseCoordinates, 
                            mapController.coordinatePerPixel
                            );
        
        fill(255);
        noStroke(); 
        textAlign(LEFT);
        text("x: " + mouse.x + " y: " + mouse.y, mouse.x+15, mouse.y+30);
        text("lat: " + cord.lat.toFixed(5) + " long: " + cord.long.toFixed(5), mouse.x+15, mouse.y+50);
        text("FPS:" + Math.round(frameRate()), mouse.x+15, mouse.y+70);
        text("Targets: " + totalTargets, mouse.x+15, mouse.y+90);

    }

}

