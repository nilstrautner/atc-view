/*
    This class contains all the targets shown on the map. It 
    accesses the altitude filter, and decides which targets to show.
    It removes old targets and handles a click on the screen, to check
    if user-target interactions are available.
*/
function TargetController () {
    this.targets = new Map();

    this.deletedTargets = new Map(); // [callsign] = last Coordinate

    /* 
        Displays all known targets depending on the selected
        altitude filter. If a target is dead, it will be deleted.

        Parameters: baseCoordinates: PVector    lat/long coordiantes at 0|0
                    coordinatePerPixel: double  similiar to zoom
    */
    this.displayAll = function(baseCoordinates, coordinatePerPixel) {
        let toDelete = [] // targets to delete later

        // Altitude Filter
        let minRange = 0;
        let maxRange = 50000;
        const rangeInput = document.querySelectorAll(".range-input input");
        rangeInput.forEach(() => {
            minRange = parseInt(rangeInput[0].value);
            maxRange = parseInt(rangeInput[1].value);
        });

        this.targets.forEach( (target) => {
            // check last update
            if (target.isDead()) {
                this.deletedTargets.set(
                    target.callsign, 
                    new Coordinate(target.pos.lat, target.pos.long)
                );
                toDelete.push(target.callsign);
                return;
            }

            // apply filter
            let opacity = 1.0;
            if (target.altitude <= minRange ||
                target.altitude >= maxRange || 
                target.altitude == "--")
                opacity = 0.3;

            // display
            target.display(baseCoordinates, coordinatePerPixel, opacity);
        });

        // Remove old targets
        toDelete.forEach( (callsign) => {
            this.targets.delete(callsign);
            // console.log("deleted", callsign)
        })
        toDelete = [];
    }

    /*
        Feeds new target data coming from an api. If a target
        already exists, it gets updated. If it doesn't, a new 
        target object will be created.

        Parameters:     dataTargets: targets[]  new target api information
    */
    this.feedData = function(dataTargets) {
        dataTargets.forEach( (target) => {
            // Invalid
            if (target.callsign === "" ||
                target.callsign === "--" ||
                target.callsign === undefined)
                return;

            // Exists
            if (this.targets.get(target.callsign) != undefined) {
                this.targets.get(target.callsign).update(target);
            } else {
                // New
                if (this.deletedTargets.get(target.callsign) == undefined || (
                    this.deletedTargets.get(target.callsign).lat !== target.pos.lat &&
                    this.deletedTargets.get(target.callsign).long !== target.pos.long) ) {
                        this.targets.set(target.callsign, target);
                    }
                // console.log("added", target.callsign)
            }
        });

        // check size
        if (this.deletedTargets.size > 50) this.deletedTargets.clear();
    }

    /*
        Handles and checks if a mouse click on a head symbol of 
        a target is in reach and changes the label position to 
        its next position.

        Parameters: mousePos:  PVector  mouse position on screen in pixels
                    baseCoordinates: PVector    lat/long coordiantes at 0|0
                    coordinatePerPixel: double  similiar to zoom
    */
    this.click = function(mousePos, baseCoordinates, coordinatesPerPixel) {
        for (const target of this.targets) {
            if (!target[1].isVisible(baseCoordinates, coordinatesPerPixel)) continue;
            
            // click on head
            // const pixels = coordinatesToPixel(target[1].pos, baseCoordinates, coordinatesPerPixel);
            // const buttonRadius = 15;
            // if ( Math.abs(mousePos.x - pixels.x) <= buttonRadius && 
            //      Math.abs(mousePos.y - pixels.y) <= buttonRadius) {
            //     target[1].labelPosId = (target[1].labelPosId + 1) % target[1].labelEdgePositions.length;
            //     break;
            // }
            if (target[1].clickLabel(mousePos)) return true;
        }

        return false;
    }

    /*
        Stops label dragging for all aircraft.
    */
    this.stopDragging = function() {
        for (const target of this.targets) {
            target[1].dragging = false;
        }
    }

    /*
       

        Parameters: mousePos:  PVector  mouse position on screen in pixels
                    baseCoordinates: PVector    lat/long coordiantes at 0|0
                    coordinatePerPixel: double  similiar to zoom
    */
    this.getTargetByMousePosition = function(mousePos, baseCoordinates, coordinatesPerPixel) {
        for (const target of this.targets) {
            if (!target[1].isVisible(baseCoordinates, coordinatesPerPixel)) continue;
            
            // click on head
            const pixels = coordinatesToPixel(target[1].pos, baseCoordinates, coordinatesPerPixel);
            const buttonRadius = 15;
            if ( Math.abs(mousePos.x - pixels.x) <= buttonRadius && 
                 Math.abs(mousePos.y - pixels.y) <= buttonRadius) {
                return target[1];
            }
            
        }

        return null;
    }

    /*
        Deletes and removes all targets.
    */
    this.deleteAll = function() {
        if (this.targets.size == 0) return;
        
        let toDelete = [];
        this.targets.forEach( (target) =>  toDelete.push(target.callsign) );
        toDelete.forEach( (callsign) => this.targets.delete(callsign) )
    }
}