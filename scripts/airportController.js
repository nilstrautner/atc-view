/*
    
*/
function AirportController ( ) {
    
    // airports
    this.airports = [ 
        new Airport("EDDF"),
        new Airport("EDDM"),
        new Airport("EDDS"),
        new Airport("EDDN"),
        new Airport("EDDK"),
        new Airport("EDDL"),
        new Airport("EDDP"),
        new Airport("EDDB"),
        new Airport("EDDC"),
        new Airport("EDDE"),
        new Airport("EDDH"),
        new Airport("EDDR"),
        new Airport("EDDV"),
        new Airport("EDDW"),
        new Airport("EDDG")
     ];

    // load all
    this.airports.forEach( (airport) => {
        airport.load();
    });
    // TODO: load dynamically

     /*
        Displays all airports, that are inside
        the screen.
     */
     this.displayAll = function(baseCoordinates, coordinatePerPixel) {
        this.airports.forEach( (airport) => {
            if (!airport.isVisible(baseCoordinates, coordinatePerPixel)) return;

            airport.display(baseCoordinates, coordinatePerPixel);
        });
     }

    /*
        Calculates the nearest airport from a pixel position 
        of the screen.
        Parameter:  pixel: PVector              center of screen
                    baseCoordinates: PVector    lat/long coordiantes at 0|0
                    coordinatePerPixel: double  similiar to zoom
        Return:     airport: string             airport icao code
    */
   this.getNearestAirport = function(pixel, baseCoordinates, coordinatesPerPixel) {
        const centerPos = pixelToCoordinates(pixel, baseCoordinates, coordinatesPerPixel);

        let distance = Infinity;
        let icao = undefined;
        this.airports.forEach(airport => {
            if (!airport.loaded) return;

            const difLat = airport.lines[0][0].lat - centerPos.lat;
            const difLong = airport.lines[0][0].long - centerPos.long;
            const dif = Math.sqrt(difLat*difLat + difLong*difLong);

            if (dif < distance) {
                distance = dif;
                icao = airport.name;
            }
        });
        return icao;
   }   
   
}