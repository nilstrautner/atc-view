# ATC RADAR - Live ADSB Air Traffic Tracker

![ATC Radar Header](sites/img/header.PNG "ATC Radar")

## What is this?
This aircraft tracker relies on adsb data to display the planes in a realistic air traffic control scheme.

A demonstration can be found on https://atc-radar.com/

## How does it work?
The program runs client side rendered and makes api requests to the free to use adsb api (https://github.com/ADSB-One) and displays the traffic via p5js.

## How to contribute
This project is open source. So feel free to report bugs, features, suggestions or code. 