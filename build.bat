@ECHO OFF

:: Basic var
SET base=%~dp0
SET dist="%base%dist"
echo Base directory: %base:~0,-1%
echo Target direcory: %dist%

:: Check if the %dist% directory exists
if exist "%dist%" (
    echo Directory %dist% exists, deleting...
    rmdir /S /Q "%dist%"
)

:: Copy folders
robocopy  "%base%scripts" "%dist%\scripts" /E
robocopy  "%base%sites" "%dist%\sites" /E
robocopy  "%base%style" "%dist%\style" /E
robocopy  "%base%img" "%dist%\img" /E
robocopy  "%base%config" "%dist%\config" /E

:: Copy files
copy "%base%\index.html" "%dist%\index.html"

echo Finish building dist directory

pause